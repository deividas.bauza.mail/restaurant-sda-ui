import { RegisterService } from 'src/app/api/services/register.service';

import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/api/models/user';

@Component({
  selector: 'app-register-user',
  templateUrl: './register-user.component.html',
  styleUrls: ['./register-user.component.css']
})
export class RegisterUserComponent implements OnInit {

  user: User = new User();

  constructor(private registerService: RegisterService) { }
  ngOnInit(): void {
    
  }

  userRegister() {
   console.log(this.user);
  this.registerService.registerUser(this.user).subscribe(data=>{
    alert("Sėkmingai užsiregistravote!")}, 
    error=>alert("Nepavyko užsiregistruoti, bandykite iš naujo.")
  );   
  } 
  }