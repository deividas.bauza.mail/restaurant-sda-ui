import { LoginService } from './../../../services/login.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/api/models/user';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent implements OnInit {

  user: User = new User();
  constructor(private loginService: LoginService) { }

  ngOnInit(): void {
  }
userLogin(){
  console.log(this.user);
  this.loginService.loginUser(this.user).subscribe(data=>{
    alert("Sėkmingai prisjunėte!")
  }, error=>alert("Nepavyko prisijungti: neteisingai nurodytas el. paštas arba slaptažodis."));
}
}
