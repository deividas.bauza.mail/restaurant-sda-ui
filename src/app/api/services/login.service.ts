import { Observable } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  appUrl="http://localhost:9090/user/login";

  constructor(private httpClient: HttpClient) { 

  }
  loginUser(user: User): Observable<Object> {
    console.log(user);
    return this.httpClient.post(`${this.appUrl}`, user);
  }

}
