import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from '../models/user';


@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  appUrl="http://localhost:9090/user";

  constructor(private httpClient: HttpClient) { }

  registerUser(user: User): Observable<Object> {
   
    return this.httpClient.post(`${this.appUrl}`, user);

  }
}