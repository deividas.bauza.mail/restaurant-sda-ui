import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './api/components/shared/header/header.component';
import { FooterComponent } from './api/components/shared/footer/footer.component';
import { RegisterUserComponent } from './api/components/ui/register-user/register-user.component';
import { HomeComponent } from './api/pages/home/home.component';
import { AboutUsComponent } from './api/pages/about-us/about-us.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { LoginUserComponent } from './api/components/ui/login-user/login-user.component';



@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    RegisterUserComponent,
   
    HomeComponent,
    AboutUsComponent,
    LoginUserComponent,
  
    
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
