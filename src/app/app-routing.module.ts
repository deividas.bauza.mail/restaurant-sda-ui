import { LoginUserComponent } from './api/components/ui/login-user/login-user.component';


import { NgModule, Component } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RegisterUserComponent } from './api/components/ui/register-user/register-user.component';

import { AboutUsComponent } from './api/pages/about-us/about-us.component';
import { HomeComponent } from './api/pages/home/home.component';

const routes: Routes = [
  {path: "api/pages/home", component: HomeComponent},
  {path: "", redirectTo: "api/pages/home", pathMatch: "full"},
  {path: "api/pages/about-us", component: AboutUsComponent},
  {path: "api/components/ui/register-user", component: RegisterUserComponent},
  {path: "api/components/ui/login-user", component: LoginUserComponent}
 
]
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
